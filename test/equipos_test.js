var superagent = require('superagent')
var expect = require('expect.js')

describe('Test equipos', function () {
  var id
  var url = 'http://localhost:3002';


  // ------------------Pruebas a la ruta Equipos------------------

  // POST /Equipos
  it('Test POST /equipos ', function (done) {
    superagent.post(url + '/equipos')
      .send({
        Placa_Inventario: "24523489",
        Nombre: "Generador",
        Descripcion: "Equipos Funcional",
        Codigo_QR: "123456",
        Ubicacion:"Almacen"
      })
      .end(function (e, res) {
        id = res.body.id;
        expect(e).to.eql(null)
        expect(res.body.status).to.eql('created')
        expect(res.body.id).to.be.an('string')
        done()
      })
  })
  // GET /Equipos
  it('Test GET /equipos', function (done) {
    superagent.get(url + '/equipos')
      .end(function (e, res) {

        expect(e).to.eql(null)
        expect(typeof res.body).to.eql('object')
        done()
      })
  })


 // GET /Equipos/:id
 it('test GET /equipos/:id', function (done) {
  superagent.get(url + '/equipos/'+id)
    .end(function (e, res) {
      expect(e).to.eql(null)
      expect(typeof res.body).to.eql('object')
      expect(res.body._id).to.eql(id)
      done()
    })
})
// PUT /Equipos/:id
it('Test PUT /equipos/:id ', function (done) {
  superagent.put(url + '/equipos/'+id)
    .send({
        Placa_Inventario: "24523489",
        Nombre: "Generador",
        Descripcion: "Equipos No Funcionales",
        Codigo_QR: "123456",
        Ubicacion:"Almacen"
    })
    .end(function (e, res) {
      expect(e).to.eql(null)
      expect(res.body.status).to.eql('updated')
      expect(res.body.equipos.Descripcion).to.eql('Equipos No Funcionales')
      done()
    })
})

// DELETE /Equipos/:id
it('Test DELETE /equipos/:id ', function (done) {
  superagent.delete(url +'/equipos/' + id)
    .end(function (e, res) {
      expect(e).to.eql(null)
      expect(typeof res.body).to.eql('object')
      expect(res.body.status).to.eql('deleted')
      done()
    })
})

})
