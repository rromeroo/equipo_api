// importar biblioteca express
var express = require('express');
// crear router
var router = express.Router();

// importar controlador de equipos
var equipos_controller = require('../controllers/equipos');

// GET /:id
router.get('/:id', equipos_controller.equipos_details);
// GET /
router.get('/', equipos_controller.equipos_all);
// POST /
router.post('/', equipos_controller.equipos_create);
// PUT /:id
router.put('/:id', equipos_controller.equipos_update);
// DELETE /:id
router.delete('/:id', equipos_controller.equipos_delete);

//export router
module.exports = router;