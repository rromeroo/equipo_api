var express = require('express');
var router = express.Router();

// Require the controllers WHICH WE DID NOT CREATE YET!!
var status_controller = require('../controllers/status');


// a simple test url to check that all of our files are communicating correctly.
router.get('/', status_controller.test);


module.exports = router;